# Intel Xe GPU Debugger (eudebug) support

This document describes how to set up a system from open-source
components to enable application debugging on supported Intel(R) GPU
devices.  These are the Xe2 devices listed in [this table][1].

You need to build and install

  * kernel mode driver (KMD) with `eudebug` support;
  * user mode driver (UMD), also known as NEO, and its subcomponents;
  * GDB with Intel GPU debug support.

## Known limitations

Backtrace does not work correctly and may stop after the first stack
frame.

Installing UMD requires sudo access to write the file at
`/etc/OpenCL/vendors/intel.icd`.

Creating a symlink to `${INSTALL_DIR}/lib/libiga64.so.2` as
`${INSTALL_DIR}/lib/libiga64.so` is necessary for the IGA library to
be detected by gdbserver's configure.  This link is created at the end
of the script given below.

We expect these limitations to be resolved in a future update.

## Build instructions

The instructions below were tested on a fresh Ubuntu 24.10 Desktop distro.
The following are the dependency packages that need to be installed
upfront, e.g. via `sudo apt install`.

```
libncurses-dev gawk flex bison openssl libssl-dev dkms libelf-dev \
libudev-dev libpci-dev libiberty-dev autoconf llvm git curl \
cmake python3-mako libdrm-dev libtool pkg-config meson \
python3-dev libmpfr-dev libgmp-dev texinfo debhelper-compat
```

You may also have to set `user.email` and `user.name` attributes in
your git config, if not already done so, for some git clone commands
to work corectly.  E.g.:
```bash
git config --global user.email "test@example.com"
git config --global user.name "test"
```

Below is a script that fetches the components and then builds and
installs them in the current directory.

```bash
#!/bin/bash
set -e

SOURCE_DIR=`pwd`/source;
BUILD_DIR=`pwd`/build;
INSTALL_DIR=`pwd`/install;
mkdir -p $SOURCE_DIR
mkdir -p $BUILD_DIR
mkdir -p $INSTALL_DIR

fetch_kmd () {
    pushd $SOURCE_DIR
    git clone -b eudebug-v3 --recursive https://gitlab.freedesktop.org/miku/kernel.git kmd
    popd
}

build_and_install_kmd () {
    KMD_BUILD_DIR=$BUILD_DIR/kmd
    mkdir -p $KMD_BUILD_DIR

    pushd $SOURCE_DIR/kmd
    curl https://gitlab.freedesktop.org/drm/xe/ci/-/raw/main/kernel/kconfig?inline=false > ${KMD_BUILD_DIR}/.config
    grep CONFIG_DRM_XE_DEBUG= ${KMD_BUILD_DIR}/.config # check for CONFIG_DRM_XE_DEBUG=y

    make O=${KMD_BUILD_DIR} olddefconfig
    make O=${KMD_BUILD_DIR} -j$(nproc) bindeb-pkg
    mv $KMD_BUILD_DIR/../*.deb $KMD_BUILD_DIR
    make O=${KMD_BUILD_DIR} prepare
    make O=${KMD_BUILD_DIR} -j$(nproc)

    sudo make O=${KMD_BUILD_DIR} modules_install
    sudo make O=${KMD_BUILD_DIR} install
    popd
}

fetch_umd () {
    # Fetch the components based on manifests/manifest.yml in NEO.

    DEP_NEO_TAG=24.52.32224.5
    DEP_IGC_VER=5b21d1b10cc02ee8f7756d1690f83792b8741235
    DEP_LLVM_VER=llvmorg-14.0.5
    DEP_OPENCL_VER=ocl-open-140
    DEP_SPIRV_LLVM_VER=llvm_release_140
    DEP_SPIRV_TOOLS_VER=main
    DEP_SPIRV_HEADERS_VER=main
    DEP_GMMLIB_VER=intel-gmmlib-22.5.5
    DEP_LIBVA_VER=v2.2-branch
    DEP_METRICS_LIB_VER=metrics-library-1.0.173
    DEP_METRICS_DISC_VER=metrics-discovery-1.13.176
    DEP_IGSC_VER=V0.9.5
    DEP_LEVEL_ZERO_VER=v1.19.2

    pushd $SOURCE_DIR

    # Intel(R) Graphics Compute Runtime for oneAPI Level Zero and OpenCL* Driver
    git clone --branch $DEP_NEO_TAG --depth 1 https://github.com/intel/compute-runtime neo

    # Intel(R) Graphics Compiler for OpenCL*
    git clone --branch master --single-branch https://github.com/intel/intel-graphics-compiler.git igc
    pushd igc
    git fetch origin $DEP_IGC_VER
    git checkout $DEP_IGC_VER
    popd

    # IGC deps
    git clone https://github.com/intel/vc-intrinsics vc-intrinsics
    git clone --branch $DEP_LLVM_VER --single-branch https://github.com/llvm/llvm-project llvm-project
    git clone --branch $DEP_OPENCL_VER --single-branch https://github.com/intel/opencl-clang llvm-project/llvm/projects/opencl-clang
    git clone --branch $DEP_SPIRV_LLVM_VER --single-branch https://github.com/KhronosGroup/SPIRV-LLVM-Translator llvm-project/llvm/projects/llvm-spirv
    git clone --branch $DEP_SPIRV_TOOLS_VER --single-branch https://github.com/KhronosGroup/SPIRV-Tools.git
    git clone --branch $DEP_SPIRV_HEADERS_VER --single-branch https://github.com/KhronosGroup/SPIRV-Headers.git

    # Intel(R) Graphics Memory Management Library
    git clone --branch $DEP_GMMLIB_VER --single-branch https://github.com/intel/gmmlib.git gmmlib

    # Video Acceleration API
    git clone --branch $DEP_LIBVA_VER --single-branch https://github.com/intel/libva.git

    # Intel(R) Metrics Library for MDAPI
    git clone --branch $DEP_METRICS_LIB_VER --single-branch https://github.com/intel/metrics-library.git

    # Intel(R) Metrics Discovery Application Programming Interface
    git clone --branch $DEP_METRICS_DISC_VER --single-branch https://github.com/intel/metrics-discovery.git

    # Intel(R) Graphics System Controller Firmware Update Library (IGSC FU)
    git clone --branch $DEP_IGSC_VER --single-branch https://github.com/intel/igsc.git igsc-lib

    # Level Zero
    git clone --branch $DEP_LEVEL_ZERO_VER --single-branch https://github.com/oneapi-src/level-zero.git

    popd
}

build_and_install_component_with_cmake () {
    COMPONENT=$1
    mkdir -p $BUILD_DIR/$COMPONENT
    pushd $BUILD_DIR/$COMPONENT
    cmake $SOURCE_DIR/$COMPONENT -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR
    make -j$(nproc)
    cmake --install .
    popd
}

build_and_install_umd () {
    build_and_install_component_with_cmake igc
    build_and_install_component_with_cmake gmmlib
    build_and_install_component_with_cmake metrics-library
    build_and_install_component_with_cmake metrics-discovery
    build_and_install_component_with_cmake level-zero

    mkdir -p $BUILD_DIR/libva
    pushd $SOURCE_DIR/libva
    ./autogen.sh --prefix $INSTALL_DIR
    make -j$(nproc)
    make install
    popd

    mkdir -p $BUILD_DIR/igsc
    pushd $BUILD_DIR/igsc
    cmake $SOURCE_DIR/igsc-lib -G Ninja -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR
    ninja -v
    cmake --install .
    popd

    mkdir -p $BUILD_DIR/neo
    pushd $BUILD_DIR/neo
    cmake -DCMAKE_BUILD_TYPE=Release \
          -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR \
          -DNEO_SKIP_UNIT_TESTS=1 \
          -DNEO_ENABLE_XE_EU_DEBUG_SUPPORT=1 \
          -DNEO_USE_XE_EU_DEBUG_EXP_UPSTREAM=1 \
          -DNEO_ENABLE_XE_PRELIM_DETECTION=0 \
          -DNEO_BUILD_OCL_PACKAGE=0 \
          -DGMM_DIR=$INSTALL_DIR \
          -DIGC_DIR=$INSTALL_DIR \
          -DLIBVA_SOURCE_DIR=$INSTALL_DIR \
          -DCMAKE_FIND_ROOT_PATH=$INSTALL_DIR \
          -DPKG_CONFIG_USE_CMAKE_PREFIX_PATH=1 \
        $SOURCE_DIR/neo
    make -j$(nproc)
    # Installation writes /etc/OpenCL/vendors/intel.icd, hence sudo.
    sudo cmake --install .
    popd
}

fetch_gdb () {
    pushd $SOURCE_DIR
    git clone --branch upstream/intelgt-mvp-plus --single-branch https://github.com/intel/gdb gdb-intelgt
    popd
}

build_and_install_gdb () {
    mkdir -p $BUILD_DIR/gdb-intelgt
    pushd $BUILD_DIR/gdb-intelgt
    $SOURCE_DIR/gdb-intelgt/configure \
                        --enable-targets="intelgt-elf" \
                        --disable-gdbserver \
                        --program-suffix="-intelgt" \
                        --prefix="${INSTALL_DIR}" \
                        --with-python \
                        --with-libiga64-prefix="${INSTALL_DIR}"
    make -j$(nproc) all-gdb
    make install-gdb
    popd

    mkdir -p $BUILD_DIR/gdbserver-intelgt
    pushd $BUILD_DIR/gdbserver-intelgt
    $SOURCE_DIR/gdb-intelgt/configure \
                            --target=intelgt-linux-ze \
                            --disable-gdb \
                            --program-suffix="-intelgt" \
                            --prefix="${INSTALL_DIR}" \
                            --with-libze_loader-prefix="${INSTALL_DIR}"
    make -j$(nproc) all-gdbserver
    make install-gdbserver
    popd
}

install_sycl () {
    # See https://www.intel.com/content/www/us/en/developer/tools/oneapi/dpc-compiler-download.html?operatingsystem=linux
    # for other installation options.
    pushd $BUILD_DIR
    wget https://registrationcenter-download.intel.com/akdlm/IRC_NAS/96aa5993-5b22-4a9b-91ab-da679f422594/intel-oneapi-base-toolkit-2025.0.0.885.sh
    chmod +x intel-oneapi-base-toolkit-2025.0.0.885.sh
    ./intel-oneapi-base-toolkit-2025.0.0.885.sh -a --silent --eula accept --components="intel.oneapi.lin.dpcpp-cpp-compiler"
    popd
}

fetch_kmd
fetch_umd
fetch_gdb

build_and_install_kmd
build_and_install_umd

# Create a symlink for autoconf to find libiga64.
ln -fs ${INSTALL_DIR}/lib/libiga64.so.2 ${INSTALL_DIR}/lib/libiga64.so

build_and_install_gdb
install_sycl
```

## Sample debug session

After building and installing the components, add your user to
the `render` and `video` groups to allow access to the GPU.
E.g.
```bash
$ sudo adduser ${USER} render
$ sudo adduser ${USER} video
```

Then reboot to the KMD.
E.g.
```bash
$ sudo reboot now
```

When rebooted, verify that the XeKMD EU Debug kernel is loaded.
E.g.
```bash
$ uname -r
6.13.0-rc1-xe+
```

Next, enable debug at the KMD level and set up the environment.
Here, we assume that the installation was made in the home
directory.  Adjust `PATH` and `LD_LIBRARY_PATH` according
to your setup.
```bash
for f in /sys/class/drm/card*/device/enable_eudebug; do echo 1 | sudo tee "$f"; done
export ZET_ENABLE_PROGRAM_DEBUGGING=1
export PATH=$PATH:${HOME}/install/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${HOME}/install/lib
source ${HOME}/intel/oneapi/setvars.sh
```

Check that the GPU device is available.
```
$ sycl-ls
[level_zero:gpu][level_zero:0] Intel(R) oneAPI Unified Runtime over Level-Zero, Intel(R) Arc(TM) Graphics ...
[opencl:cpu][opencl:0] Intel(R) OpenCL, Intel(R) Core(TM) ...
[opencl:gpu][opencl:1] Intel(R) OpenCL Graphics, Intel(R) Arc(TM) Graphics ...
```

Compile a sample SYCL program from the GDB testsuite.
```
$ icpx -fsycl -g -O0 ${HOME}/source/gdb-intelgt/gdb/testsuite/gdb.sycl/parallel-for-1D.cpp -o parallel-for-1D
$ ./parallel-for-1D gpu Intel level_zero
SYCL: Using device: [Intel(R) ...] from [Intel(R) Level-Zero] version [...]
Correct
```

Run a sample debug session.
```
   $ gdb-intelgt -q --args ./parallel-for-1D gpu Intel level_zero
   Reading symbols from ./parallel-for-1D...
   (gdb) break 56
   Breakpoint 1 at 0x406a7f: file /<path>/gdb-intelgt/gdb/testsuite/gdb.sycl/parallel-for-1D.cpp, line 56.
   (gdb) run
   ...
   SYCL: Using device: [Intel(R) ...] from [Intel(R) Level-Zero] version [...]
   Attached; given pid = 8888, updated to 1
   Remote debugging using stdio

   intelgt: Started gdbserver for process 8888 with inferior 2.
   intelgt: It is recommended to do 'set schedule-multiple on'.
   intelgt: Type 'continue' to resume.
   [Switching to thread 1.1 (Thread 0x7ffff7697540 (LWP 8888))]
   (gdb) set schedule-multiple on
   (gdb) continue
   Continuing.
   [New Thread 0x7fffdab27640 (LWP 8901)]
   [Switching to thread 2.110:0 (Thread 1.110 lane 0)]

   Thread 2.110 hit Breakpoint 1.2, with SIMD lanes [0-15], main::{lambda(sycl::_V1::handler&)#1}::operator()(sycl::_V1::handler&) const::{lambda(sycl::_V1::id<1>)#1}::operator()(sycl::_V1::id<1>) const (this=0xff0000000037a710, wiID=...) at parallel-for-1D.cpp:56
   56                  int in_elem2 = accessorIn[dim0];
   (gdb) info locals
   dim0 = 416
   in_elem = 539
   in_elem2 = 3649936
   (gdb) set scheduler-locking on
   (gdb) next
   57                  accessorOut[wiID] = in_elem + 100; /* kernel-last-line */
   (gdb) info locals
   dim0 = 416
   in_elem = 539
   in_elem2 = 539
   (gdb) thread :5
   [Switching to thread 2.110:5 (Thread 1.110 lane 5)]
   #0  main::{lambda(sycl::_V1::handler&)#1}::operator()(sycl::_V1::handler&) const::{lambda(sycl::_V1::id<1>)#1}::operator()(sycl::_V1::id<1>) const (
       this=0xff0000000037a850, wiID=...) at parallel-for-1D.cpp:57
   57                  accessorOut[wiID] = in_elem + 100; /* kernel-last-line */
   (gdb) info locals
   dim0 = 421
   in_elem = 544
   in_elem2 = 544
   (gdb) print get_dim(wiID, 0)
   $1 = 421
   (gdb)
```

---

[1]: https://dgpu-docs.intel.com/devices/hardware-table.html#gpus-with-supported-drivers
